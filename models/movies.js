'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class movies extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      movies.belongsTo(models.genre)
      movies.belongsToMany(models.users, {through : 'models.review'})
    }
  };
  movies.init({
    title: {
      type : DataTypes.STRING,
      validate :{
        notEmpty: {
          msg : "Please input the title.",
        }
      }
    },
    picture:{
      type : DataTypes.STRING,
      validate :{
        notEmpty: {
          msg : "Please input the picture here."
        }
      }
    },
    trailer:{
      type : DataTypes.STRING,
      validate : {
        isUrl : true
      }
    },
    year: {
      type: DataTypes.INTEGER,
      validate : { 
        isNumeric: true,
        notEmpty: {
          msg : "Please input the year.",
        }
      }
    },
    synopsis: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Please input the synopsis here."
        }
      }
    },
    characters: {
      type: DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Please input the characters here."
        }
      }
    },
    genreId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'movies',
  });
  return movies;
};  
