const { movies, genre, users, review } = require('../models')

class ReviewController {
    static async getReview(req,res, next) {
        try {
            const result = await review.findAll({
                order: [['id', 'ASC']],
                include : [
                    users
                ]
            })
            res.status(200).json(result)
        } catch (error) {
            next(error)
        }
    }
    static async addReview(req,res, next) {
        const { comment, rating, userId, movieId } = req.body;
        try {
                const newReview = await review.create({
                    comment, 
                    rating,
                    userId,
                    movieId
                })
                res.status(200).json(newReview)
        } catch (error) {
            next(error)
        }
    }
    static async deleteReview(req,res, next) {
        const id = req.params.id;
        try {
            const result = await review.destroy({
                where: {
                    id
                }
            })
            res.status(200).json({result, msg : 'Review deleted'})
        } catch (error) {
            next(error)
        }
    }
    static async updateReview(req,res, next) {
        const id = req.params.id;
        const { comment, rating, userId, movieId } = req.body;
        try {
            const result = await review.findOne({
                where :{
                    id,
                }
            })
            if (result) {
                const editReview = await review.update({
                    comment, 
                    rating,
                    userId,
                    movieId
                })
                res.status(200).json({editReview, msg : 'Review updated'})
            } else {
                res.status(404).json({msg: 'Review is not found.'})    
            }
            
        } catch (error) {
            next(error)
        }
    }
    static async getReviewbyMovie(req,res, next) {
        const movieId = req.params.movieId
        try {
            const foundMovie = await movies.findOne({
                where: { 
                    id : movieId
                }
            })
            const result = await review.findAll({
                where : { 
                    movieId 
                },
                order: [['id', 'ASC']],
                attributes: ['comment', 'rating'],
                include : {
                    model: users
                }
            });
            let reviewUser = [];
            result.forEach(comment => {
                reviewUser.push(comment.users)
            });
            //Rata2 rating
            let sumrating = 0;
            sumrating = result.reduce((a, b) => a.rating + b.rating, 0);
            let totalrating = sumrating / result.length;

            //let comment = paginate()
            res.status(200).json({
                "Movie" : movies.title,
                "Rating" : totalrating,
                "Review" : comment
            })
        } catch (error) {
            next(error)
        }
    }
}

module.exports = ReviewController;