const { movies, genre, users } = require('../models');
const { Op } = require('sequelize');

class MovieController {
    static async getMovie (req,res, next){
        try {
            const result = await movies.findAll({
                order: [['id', 'ASC']],
                include : [
                    genre
                ]
            })
            res.status(200).json(result)
        } catch (error) {
            next(error)
        }
    }
    static async addMovie (req,res, next){
        const { title, picture, trailer, year, synopsis, characters, genreId, userId } = req.body;
        try {
            const result = await movies.findOne({
                where: {
                    title
                }
            })
            if (result) {
                res.status(409).json({msg: 'Movie already exists.'})    
            } else {
                const newMovie = await movies.create({
                    title, 
                    picture, 
                    trailer, 
                    year, 
                    synopsis, 
                    characters, 
                    genreId, 
                    userId
                })
                res.status(201).json(newMovie)
            }
            
        } catch (error) {
            next(error)
        }
    }
    static async deleteMovie (req,res, next){
        const id = req.params.id;
        try {
            const result = await movies.destroy({
                where: {
                    id
                }
            })
            res.status(200).json({msg : 'Movie deleted'})
        } catch (error) {
            next(error)
        }
    }

    static async updateMovie (req,res, next){
        const id = req.params.id;
        const { title, picture, trailer, year, synopsis, characters, genreId, userId } = req.body;
        try {
                const editMovie = await movies.update({
                   title, picture, trailer, year, synopsis, characters, genreId, userId
                }, 
                {where : {
                    id
                }
            })
                res.status(200).json({editMovie, msg : 'Movie updated'})
            
        } catch (error) {
            next(error)
        }
    }
    static async findMoviebyId (req,res, next){
        const id = req.params.id;
        try {
            const result = await movies.findOne({
                where :{
                    id,
                }
            })
            if (result) {
                res.status(200).json(result)
            } else {
                res.status(404).json({msg: 'Movie is not found.'})    
            }
            
        } catch (error) {
            next(error)
        }
    }
    static async findMoviebytitle (req,res, next){
        const { title } = req.body;
        try {
            const result = await movies.findOne({
                where :{
                    title : {[Op.iLike]: title}
                }
            })
            if (result) {
                const found = await movies.findAll({
                    where :{
                        title : {[Op.iLike]: title}
                    }
                })
                res.status(200).json(result)
            } else {
                res.status(404).json({msg: 'Movie is not found.'})    
            }
            
        } catch (error) {
            next(error)
        }
    }
}

module.exports = MovieController;