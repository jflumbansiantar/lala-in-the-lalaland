const jwt = require("jsonwebtoken");
const secretKey = "2020";

const tokenGenerator = (users) => {
  console.log(users, "--user");
  const { email, role } = users;

  return jwt.sign(
    {
      email,
      role,
    },
    secretKey
  );
};

const tokenVerifier = (access_token) => {
  return jwt.verify(access_token, secretKey);
};

module.exports = {
  tokenGenerator,
  tokenVerifier,
};