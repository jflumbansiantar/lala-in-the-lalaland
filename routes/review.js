const { Router } = require('express');
const router = Router();
const ReviewController = require('../controllers/review')

// const { authentication, authorization } = require('../middlewares/auth')

router.get('/', ReviewController.getReview)
router.post('/', ReviewController.addReview)
router.delete('/:id', ReviewController.deleteReview)
router.put('/update/:id', ReviewController.updateReview)


module.exports = router;
