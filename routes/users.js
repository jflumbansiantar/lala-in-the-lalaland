const { Router } = require('express');
const router = Router();
const usersController = require('../controllers/users')
const { upload } = require('../middlewares/multer')

router.get('/', usersController.getAllUsers)
router.post('/login', usersController.login)
router.post('/register', upload.single('image'), usersController.register)
router.put('/edit/:id', usersController.editUsers)
router.get('/find/:id', usersController.findById)

module.exports = router;
