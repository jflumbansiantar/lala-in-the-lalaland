const { Router } = require('express');
const router = Router();
const GenreController = require('../controllers/genre')

router.get('/', GenreController.getGenre) 
router.post('/', GenreController.addGenre)
router.delete('/:genre', GenreController.deleteGenre)

module.exports = router;
