const { Router } = require('express');
const router = Router();
const { MovieController } = require('../controllers/movies')
const { authentication, authorization } = require('../middlewares/auth');
const auth = require('../middlewares/auth');
const { upload } = require('../middlewares/multer')

router.get('/', authentication, authorization, MovieController.getMovie)
router.post('/',authentication, authorization, upload.single('picture'), MovieController.addMovie)
router.get('/:id',authentication, authorization, MovieController.findById)
router.delete('/:id', authentication, authorization, MovieController.deleteMovie)
router.put('/:id', authentication, authorization, MovieController.updateMovie) 
router.get('/:id', authentication, authorization, MovieController.findMoviebyId) 
router.get('/:id', authentication, authorization, MovieController.findMoviebytitle) 

module.exports = router;


