const { Router } = require('express');
const router = Router();
const usersRoutes = require('./users')
const moviesRoutes = require('./movies')
const genreRoutes = require('./genre')

router.get('/', (req,res)=>{
    res.status(200).json({
        message : "Welcome to the Home Page Movie Review Apps!"
    })
});
router.use('/movie', moviesRoutes)
router.use('/users', usersRoutes)
router.use('/genre', genreRoutes)

module.exports = router;